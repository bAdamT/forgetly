import { z } from "zod";

export const Todo = z.object({
  id: z.number(),
  description: z.string(),
  created_at: z.coerce.date(),
  completed_at: z.coerce.date().nullable(),
  user_id: z.string().uuid(),
});

export const NewTodo = Todo.omit({ id: true, created_at: true, user_id: true });

export type Todo = z.infer<typeof Todo>;
export type NewTodo = z.infer<typeof NewTodo>;


