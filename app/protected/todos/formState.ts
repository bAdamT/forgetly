import { Todo } from "@/models/todo";
import { todoAction } from "./actions";

export type TodoFormState = {
  message?: string;
  outcome?: "error" | "success";
  pending?: boolean;
  todos: Todo[];
};

export type TodoAction = 
  | { type: "create"; description: string }
  | { type: "complete"; todoId: number }
  | { type: "dismiss"; todoId: number };

export type OptimisticAction = 
  | { type: "create"; description: string }
  | { type: "complete"; todoId: number }
  | { type: "dismiss"; todoId: number };

export function actionReducer(state: TodoFormState, action: TodoAction) {
  if (action.type === "create") {
    return todoAction(state, { type: "create", description: action.description });
  } else if (action.type === "complete") {
    return todoAction(state, { type: "complete", todoId: action.todoId });
  } else {
    return todoAction(state, { type: "dismiss", todoId: action.todoId });
  }
}

export function optimisticReducer(state: TodoFormState, action: OptimisticAction) {
  if (action.type === "create") {
    const optimisticTodo: Todo = {
      id: Math.random(),
      description: action.description,
      created_at: new Date(),
      completed_at: null,
      user_id: "optimistic",
    };

    return {
      ...state,
      pending: true,
      todos: [optimisticTodo, ...state.todos],
    };
  } else if (action.type === "complete") {
    return {
      ...state,
      todos: state.todos.map((todo) =>
        todo.id === action.todoId
          ? { ...todo, completed_at: new Date() }
          : todo
      ),
    };
  } else {
    return {
      ...state,
      todos: state.todos.filter((todo) => todo.id !== action.todoId),
    };
  }
}
