"use server";

import { createClient } from "@/utils/supabase/server";
import { Todo } from "@/models/todo";
import { z } from "zod";

export type TodoFormState = {
  message?: string;
  outcome?: "error" | "success";
  pending?: boolean;
  todos: Todo[];
};

type TodoActionType = 
  | { type: "create"; description: string }
  | { type: "complete"; todoId: number }
  | { type: "dismiss"; todoId: number };

export async function todoAction(
  prevState: TodoFormState,
  action: TodoActionType
): Promise<TodoFormState> {
  const supabase = await createClient();

  const { data: user } = await supabase.auth.getUser();
  if (!user.user) {
    return {
      outcome: "error",
      message: "Not authenticated",
      todos: prevState.todos,
    };
  }

  if (action.type === "create") {
    if (!action.description) {
      return {
        outcome: "error",
        message: "Description is required",
        todos: prevState.todos
      };
    }

    const { data, error } = await supabase
      .from("todos")
      .insert([{ 
        description: action.description,
        user_id: user.user.id
      }])
      .select()
      .single();

    if (error) {
      console.error("Supabase error:", error.code, error.message, error.details);
      return {
        outcome: "error",
        message: `Failed to create todo: ${error.message}`,
        todos: prevState.todos,
      };
    }

    const newTodo = Todo.safeParse(data);
    if (!newTodo.success) {
      return {
        outcome: "error",
        message: "Failed to parse todo",
        todos: prevState.todos,
      };
    }

    return {
      outcome: "success",
      message: "Todo created successfully",
      todos: [newTodo.data, ...prevState.todos],
    };
  } else if (action.type === "complete") {
    const { data, error } = await supabase
      .from("todos")
      .update({ completed_at: new Date() })
      .eq("id", action.todoId)
      .eq("user_id", user.user.id)
      .select()
      .single();

    if (error) {
      return {
        outcome: "error",
        message: error.message,
        todos: prevState.todos,
      };
    }
    
    const updatedTodo = Todo.safeParse(data);
    if (!updatedTodo.success) {
      return {
        outcome: "error",
        message: "Failed to parse todo",
        todos: prevState.todos,
      };
    }

    return {
      outcome: "success",
      message: "Todo completed",
      todos: prevState.todos.map((todo) =>
        todo.id === action.todoId ? updatedTodo.data : todo
      ),
    };
  } else if (action.type === "dismiss") {
    const { error } = await supabase
      .from("todos")
      .delete()
      .eq("id", action.todoId)
      .eq("user_id", user.user.id);

    if (error) {
      return {
        outcome: "error",
        message: error.message,
        todos: prevState.todos,
      };
    }

    return {
      outcome: "success",
      message: "Todo dismissed",
      todos: prevState.todos.filter((todo) => todo.id !== action.todoId),
    };
  }

  return {
    outcome: "error",
    message: "Invalid action type",
    todos: prevState.todos,
  };
}

export async function getTodos() {
  const supabase = await createClient();
  
  const { data: user } = await supabase.auth.getUser();
  if (!user.user) {
    throw new Error("Not authenticated");
  }

  // Get start of today in UTC
  const startOfToday = new Date();
  startOfToday.setUTCHours(0, 0, 0, 0);

  const { data: todos, error } = await supabase
    .from("todos")
    .select("*")
    .eq("user_id", user.user.id)
    .or(`completed_at.is.null,completed_at.gte.${startOfToday.toISOString()}`)
    .order("created_at", { ascending: false });

  if (error) {
    console.error("Supabase error:", error.code, error.message, error.details);
    throw new Error(`Failed to fetch todos: ${error.message}`);
  }

  return z.array(Todo).parse(todos);
} 