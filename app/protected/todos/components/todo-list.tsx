"use client";

import { cn } from "@/lib/utils";
import { Todo } from "@/models/todo";
import { Check, X } from "lucide-react";
import { startTransition, useActionState, useOptimistic } from "react";

import { FormMessage, Message } from "@/components/form-message";
import { SubmitButton } from "@/components/submit-button";
import { Button } from "@/components/ui/button";
import { Card } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";

import {
  type OptimisticAction,
  type TodoFormState,
  actionReducer,
  optimisticReducer,
} from "../formState";

export function TodoList({ initialTodos }: { initialTodos: Todo[] }) {
  const [state, formAction] = useActionState(actionReducer, {
    todos: initialTodos,
  });

  const [optimisticState, optimisticUpdate] = useOptimistic<
    TodoFormState,
    OptimisticAction
  >(state, optimisticReducer);

  function getFormMessage(state: TodoFormState): Message | null {
    if (!state.message || !state.outcome) return null;

    if (state.outcome === "error") {
      return { error: state.message };
    } else if (state.outcome === "success") {
      return { success: state.message };
    } else {
      return { message: state.message };
    }
  }

  async function createTodo(formData: FormData) {
    const description = formData.get("description") as string;
    optimisticUpdate({
      type: "create",
      description,
    });
    await formAction({ type: "create", description });
  }

  async function completeTodo(todoId: number) {
    optimisticUpdate({
      type: "complete",
      todoId,
    });
    await formAction({ type: "complete", todoId });
  }

  async function dismissTodo(todoId: number) {
    optimisticUpdate({
      type: "dismiss",
      todoId,
    });
    await formAction({ type: "dismiss", todoId });
  }

  function isBeforeToday(date: Date) {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    return date < today;
  }

  const formMessage = getFormMessage(state);

  return (
    <div className="flex w-full flex-col">
      <div className="sticky top-16 z-10 flex w-full flex-col border-b bg-background">
        <div className="flex w-full justify-center p-6">
          <div className="w-full max-w-md">
            <form action={createTodo} className="flex flex-col gap-2">
              <Label htmlFor="description">New Todo</Label>
              <div className="flex gap-2">
                <Input
                  id="description"
                  name="description"
                  type="text"
                  required
                  placeholder="What needs to be done?"
                />
                <SubmitButton className="w-[100px]">Add</SubmitButton>
              </div>
              <div className="h-8">
                {formMessage && <FormMessage message={formMessage} />}
              </div>
            </form>
          </div>
        </div>
      </div>

      <div className="flex flex-1 flex-col gap-8 p-8">
        <div className="grid w-full grid-cols-1 gap-4 lg:grid-cols-2">
          {optimisticState.todos.map((todo: Todo) => {
            const isPastDeadline =
              isBeforeToday(todo.created_at) && !todo.completed_at;
            return (
              <Card
                key={todo.id}
                className={cn(
                  "group relative p-5 transition-all duration-200",
                  todo.user_id === "optimistic" && "opacity-50",
                  todo.completed_at && "bg-muted",
                )}
              >
                {!todo.completed_at && (
                  <Button
                    variant="ghost"
                    size="icon"
                    className="absolute right-1 top-1 h-7 w-7 opacity-0 transition-opacity hover:bg-destructive hover:text-destructive-foreground group-hover:opacity-100"
                    onClick={() => {
                      startTransition(() => dismissTodo(todo.id));
                    }}
                    disabled={todo.user_id === "optimistic"}
                  >
                    <X className="h-4 w-4" />
                  </Button>
                )}
                <div className="flex min-h-[64px] flex-col gap-2.5">
                  <div className="flex items-center gap-3 pr-8">
                    <Button
                      variant="ghost"
                      size="icon"
                      className={cn(
                        "h-6 w-6 shrink-0 rounded-full border",
                        todo.completed_at &&
                          "bg-primary text-primary-foreground",
                      )}
                      onClick={() => {
                        if (!todo.completed_at) {
                          startTransition(() => completeTodo(todo.id));
                        }
                      }}
                      disabled={
                        todo.user_id === "optimistic" ||
                        todo.completed_at !== null
                      }
                    >
                      {todo.completed_at && <Check className="h-3 w-3" />}
                    </Button>
                    <span
                      className={cn(
                        "leading-tight",
                        todo.user_id === "optimistic" &&
                          "text-muted-foreground",
                        todo.completed_at &&
                          "text-muted-foreground line-through",
                      )}
                    >
                      {todo.description}
                    </span>
                  </div>
                  <div className="absolute bottom-2 right-4 flex justify-end">
                    <span
                      className={cn(
                        "rounded px-1",
                        isPastDeadline ? "text-[18px]" : "text-[11px]",
                        isPastDeadline
                          ? "bg-destructive/10 font-medium text-destructive"
                          : "text-muted-foreground/75",
                      )}
                    >
                      {todo.created_at.toLocaleDateString()}
                    </span>
                  </div>
                </div>
              </Card>
            );
          })}
          {optimisticState.todos.length === 0 && (
            <p className="col-span-full text-center text-muted-foreground">
              No todos yet
            </p>
          )}
        </div>
      </div>
    </div>
  );
}
