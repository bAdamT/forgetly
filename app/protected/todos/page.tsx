import { getTodos } from "./actions";
import { TodoList } from "./components/todo-list";

export default async function TodosPage() {
  const todos = await getTodos();

  return (
    <div className="flex w-full flex-col gap-8 p-4">
      <h1 className="text-2xl font-medium">Todos</h1>
      <TodoList initialTodos={todos} />
    </div>
  );
}
